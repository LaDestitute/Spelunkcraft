# Spelunkcraft
Mod Discord Server:

https://discord.gg/CJhWB3F

Mod Trello:

https://trello.com/b/NNrIF0j7/spelunkcraft

This is a mod that brings elements and mechanics from Spelunky, such as Underground Jungles, Ice Caves, Olmec, etc. Spelunky 2 content is also planned, with the mod currently in the process of being ported to 1.16.
The last version of this mod was for 1.6.4 way back in 2013 so the initial 1.12.2 port was completely from scratch.

Please keep note that most of the development changes occur on the respective current-unstable branch, and so things may be unfinished, have bugs, etc.
For bug reports, please submit them to the Issues part of the repo.

For use with GitHub Desktop + Eclipse/IntelliJ.
If you wish to help out with development or fork and push-request fixes or improvements:
1) clone the repo via GitHub Desktop/etc using the HTTPS link to a local folder
2) open up cmd
3) navigate to the src folder where it was cloned
4) i.e on Windows: "cd /d C:\Desktop\Spelunkcraft"
5) depending on if you use Eclipse or IntelliJ, run the following:
6) if you use Eclipse, run "gradlew genEclipseRuns"
7) if you use IntelliJ, run "gradlew genIntellijRuns"
8) let it finish
9) done! Open the mod's src in Eclipse/IntelliJ/etc :)

On a small note, keep note that the online commit-timestamps are UTC based while my local timezone is Pacific Standard Time.