package ladestitute.spelunkcraft.event;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.entities.ammo.EntityPistolBullet;
import ladestitute.spelunkcraft.registries.ItemInit;
import ladestitute.spelunkcraft.registries.SoundInit;
import ladestitute.spelunkcraft.util.helpers.KeyboardHelper;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = Spelunkcraft.MOD_ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class SpelunkcraftItemEventHandler {

    //Climbing gloves event
    @SubscribeEvent // LivingEntity#func_233580_cy_c() ----> LivingEntity#getPosition()
    public static void ClimbingGloves(TickEvent.PlayerTickEvent event) {
        LivingEntity player = event.player;
        if (KeyboardHelper.isHoldingZ() && player.getHeldItemOffhand().getItem() == ItemInit.CLIMBING_GLOVES.get() && player.collidedHorizontally) {
           // Spelunkcraft.LOGGER.info("Player tried to jump with climbing gloves equipped!");
            World world = player.getEntityWorld();
            player.setMotion(0, 0.20, 0);
        }
    }

    //Jetpack event
    @SubscribeEvent // LivingEntity#func_233580_cy_c() ----> LivingEntity#getPosition()
    public static void Jetpack(TickEvent.PlayerTickEvent event) {
        LivingEntity playerIn = event.player;
        if (KeyboardHelper.isHoldingZ() && event.player.inventory.armorItemInSlot(2).getItem() == ItemInit.JETPACK.get()) {
            // Spelunkcraft.LOGGER.info("Player tried to jump with climbing gloves equipped!");
            //playerIn.addPotionEffect(new EffectInstance(Effects.LEVITATION, 20, 1));
            playerIn.addPotionEffect(new EffectInstance(Effects.SLOW_FALLING, 20, 1));
            World world = playerIn.getEntityWorld();
            if(playerIn.getHorizontalFacing().equals(Direction.NORTH)) {
                playerIn.setMotion(0, 0.40, -0.20);
                playerIn.getHeldItemOffhand().getItem().damageItem(playerIn.getHeldItemOffhand().getStack(), 1, playerIn, player -> {
                });
            }
            if(playerIn.getHorizontalFacing().equals(Direction.SOUTH)) {
                playerIn.setMotion(0, 0.40, 0.20);
                playerIn.getHeldItemOffhand().getItem().damageItem(playerIn.getHeldItemOffhand().getStack(), 1, playerIn, player -> {
                });
            }
            if(playerIn.getHorizontalFacing().equals(Direction.WEST)) {
                playerIn.setMotion(-0.20, 0.40, 0);
                playerIn.getHeldItemOffhand().getItem().damageItem(playerIn.getHeldItemOffhand().getStack(), 1, playerIn, player -> {
                });
            }
            if(playerIn.getHorizontalFacing().equals(Direction.EAST)) {
                playerIn.setMotion(0.20, 0.40, 0);
                playerIn.getHeldItemOffhand().getItem().damageItem(playerIn.getHeldItemOffhand().getStack(), 1, playerIn, player -> {
                });
            }
        }
    }

    // Spring Shoes sound-effect event
    @SubscribeEvent
    public static void SpringShoesEvent(LivingEvent.LivingJumpEvent event)
    {
        ItemStack bootsStack = event.getEntityLiving().getItemStackFromSlot(EquipmentSlotType.FEET);
        if (bootsStack.getItem() == ItemInit.SPRING_SHOES.get()) {
            event.getEntityLiving().playSound(SoundInit.SPRING.get(), 1, 1);
            AbstractBlock.Properties.create(Material.ROCK).setLightLevel(blockState ->
                    12);
        }
    }

    @SubscribeEvent
    public void meleesounds(PlayerInteractEvent.RightClickEmpty event)
    {
        ItemStack handStack = event.getEntityLiving().getHeldItemMainhand();
        if (handStack.getItem() == ItemInit.PISTOL.get())
        {
            event.getPlayer().playSound(SoundInit.RANGED_SHOTGUN.get(), 0.5F, 1.5F);
        }
    }


    @SubscribeEvent
    public static void GunSounds(LivingSpawnEvent event)
    {
        if (event.getEntity() instanceof EntityPistolBullet)
        {
            event.getEntityLiving().playSound(SoundInit.RANGED_SHOTGUN.get(), 0.5F, 1.5F);
        }
    }

}
