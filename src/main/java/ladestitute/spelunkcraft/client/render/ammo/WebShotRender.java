package ladestitute.spelunkcraft.client.render.ammo;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.entities.ammo.EntityWebShot;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class WebShotRender extends ArrowRenderer<EntityWebShot> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(Spelunkcraft.MOD_ID, "textures/entity/ammo/web_shot.png");

    public WebShotRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getEntityTexture(EntityWebShot entity) {
        return TEXTURE;
    }


}