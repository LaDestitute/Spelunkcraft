package ladestitute.spelunkcraft.client.render.ammo;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.entities.ammo.EntityPistolBullet;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PistolBulletRender extends ArrowRenderer<EntityPistolBullet> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(Spelunkcraft.MOD_ID, "textures/entity/ammo/pistol_bullet.png");

    public PistolBulletRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getEntityTexture(EntityPistolBullet entity) {
        return TEXTURE;
    }


}