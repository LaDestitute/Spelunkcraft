package ladestitute.spelunkcraft.client.render.ammo;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.entities.ammo.EntityFreezeBolt;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class FreezeBoltRender extends ArrowRenderer<EntityFreezeBolt> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(Spelunkcraft.MOD_ID, "textures/entity/ammo/freeze_bolt.png");

    public FreezeBoltRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getEntityTexture(EntityFreezeBolt entity) {
        return TEXTURE;
    }


}
