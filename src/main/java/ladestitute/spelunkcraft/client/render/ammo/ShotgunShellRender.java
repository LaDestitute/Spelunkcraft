package ladestitute.spelunkcraft.client.render.ammo;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.entities.ammo.EntityShotgunShell;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ShotgunShellRender extends ArrowRenderer<EntityShotgunShell> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(Spelunkcraft.MOD_ID, "textures/entity/ammo/shotgun_shell.png");

    public ShotgunShellRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getEntityTexture(EntityShotgunShell entity) {
        return TEXTURE;
    }


}