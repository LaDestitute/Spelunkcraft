package ladestitute.spelunkcraft.util.tags;

import ladestitute.spelunkcraft.registries.BlockInit;
import ladestitute.spelunkcraft.registries.ItemInit;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.ItemTagsProvider;
import net.minecraft.item.Items;
import net.minecraftforge.common.data.ExistingFileHelper;
import ladestitute.spelunkcraft.Spelunkcraft;

public class TagsDataGen {
    public static class ItemTagsDataGen extends ItemTagsProvider {

        public ItemTagsDataGen(DataGenerator generatorIn, BlockTagsProvider blockTagsProvider,
                               ExistingFileHelper existingFileHelper) {
            super(generatorIn, blockTagsProvider, Spelunkcraft.MOD_ID, existingFileHelper);
        }

        @Override
        protected void registerTags() {
           // getOrCreateBuilder(SpelunkTags.Items.GEMS).addTag(SpelunkTags.Items.GEMS_RUBY);
            getOrCreateBuilder(SpelunkTags.Items.GEMS).addTag(SpelunkTags.Items.GEMS_RUBY);
            getOrCreateBuilder(SpelunkTags.Items.GEMS_RUBY).add(ItemInit.LARGE_RUBY.get());
            getOrCreateBuilder(SpelunkTags.Items.GEMS_RUBY).add(ItemInit.SMALL_RUBY.get());
        }
    }

    public static class BlockTagsDataGen extends BlockTagsProvider {

        public BlockTagsDataGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
            super(generatorIn, Spelunkcraft.MOD_ID, existingFileHelper);
        }

        @Override
        protected void registerTags() {
            getOrCreateBuilder(SpelunkTags.Blocks.ORES).addTag(SpelunkTags.Blocks.ORES_RUBY);
        //    getOrCreateBuilder(SpelunkTags.Blocks.ORES_RUBY).add(BlockInit.ORERUBY.get());
        }
    }
}