package ladestitute.spelunkcraft.util.enums;

import java.util.function.Supplier;

import ladestitute.spelunkcraft.Spelunkcraft;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public enum ModArmorMaterials implements IArmorMaterial {

    CLIMBING_GLOVES(Spelunkcraft.MOD_ID + ":climbing_gloves", 25, new int[] { 0, 18, 0, 0 }, 313,
            SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0, () -> { return null; },0),

    SPRING_SHOES(Spelunkcraft.MOD_ID + ":spring_shoes", 25, new int[] { 0, 0, 0, 18 }, 313,
            SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0, () -> { return null; },0),

    SPECTACLES(Spelunkcraft.MOD_ID + ":spectacles", 25, new int[] { 15, 0, 0, 0 }, 265,
            SoundEvents.ITEM_ARMOR_EQUIP_IRON, 0, () -> { return null; },0),

    PITCHERS_MITT(Spelunkcraft.MOD_ID + ":pitchers_mitt", 23, new int[] { 0, 23, 0, 0 }, 385,
            SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0, () -> { return null; },0),

    CAPE(Spelunkcraft.MOD_ID + ":cape", 23, new int[] { 0, 23, 0, 0 }, 385,
    SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0, () -> { return null; },0),

    JETPACK(Spelunkcraft.MOD_ID + ":jetpack", 23, new int[] { 0, 1, 0, 0 }, 906,
            SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 0, () -> { return null; },0),

    PARACHUTE(Spelunkcraft.MOD_ID + ":parachute", 1, new int[] { 0, 23, 0, 0 }, 385,
            SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, 0, () -> { return null; },0);

    private static final int[] MAX_DAMAGE_ARRAY = new int[] { 11, 16, 15, 13 };
    private final String name;
    private final int maxDamageFactor; //Durability, Iron=15, Diamond=33, Gold=7, Leather=5
    private final int[] damageReductionAmountArray; //Armor Bar Protection, 1 = 1/2 armor bar
    private final int enchantability;
    private final SoundEvent soundEvent;
    private final float toughness; //Increases Protection, 0.0F=Iron/Gold/Leather, 2.0F=Diamond, 3.0F=Netherite
    private final Supplier<Ingredient> repairMaterial;
    private final float knockbackResistance; //1.0F=No Knockback, 0.0F=Disabled

    ModArmorMaterials(String name, int maxDamageFactor, int[] damageReductionAmountArray, int enchantability,
                     SoundEvent soundEvent, float toughness, Supplier<Ingredient> repairMaterial, float knockbackResistance) {
        this.name = name;
        this.maxDamageFactor = maxDamageFactor;
        this.damageReductionAmountArray = damageReductionAmountArray;
        this.enchantability = enchantability;
        this.soundEvent = soundEvent;
        this.toughness = toughness;
        this.repairMaterial = repairMaterial;
        this.knockbackResistance = knockbackResistance;
    }

    @Override
    public int getDurability(EquipmentSlotType slotIn) {
        return MAX_DAMAGE_ARRAY[slotIn.getIndex()] * this.maxDamageFactor;
    }

    @Override
    public int getDamageReductionAmount(EquipmentSlotType slotIn) {
        return this.damageReductionAmountArray[slotIn.getIndex()];
    }

    @Override
    public int getEnchantability() {
        return this.enchantability;
    }

    @Override
    public SoundEvent getSoundEvent() {
        return this.soundEvent;
    }

    @Override
    public Ingredient getRepairMaterial() {
        return this.repairMaterial.get();
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public float getToughness() {
        return this.toughness;
    }

    @Override
    public float getKnockbackResistance() {
        return 0;
    }

}