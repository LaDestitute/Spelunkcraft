package ladestitute.spelunkcraft.util;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.client.render.ammo.FreezeBoltRender;
import ladestitute.spelunkcraft.client.render.ammo.PistolBulletRender;
import ladestitute.spelunkcraft.client.render.ammo.ShotgunShellRender;
import ladestitute.spelunkcraft.client.render.ammo.WebShotRender;
import ladestitute.spelunkcraft.registries.BlockInit;
import ladestitute.spelunkcraft.registries.EntityInit;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = Spelunkcraft.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {

    @SubscribeEvent
    public static void clientSetupEvent(FMLClientSetupEvent event){
        RenderTypeLookup.setRenderLayer(BlockInit.SPIKES.get(), RenderType.getCutout());
        //Render registering, so far only ammo and projectiles (rocks are still being tested)
        //RenderingRegistry.registerEntityRenderingHandler(EntitiesInit.PROJECTILE_ROCK.get(), ProjectileRockRender::new);
      //  RenderingRegistry.registerEntityRenderingHandler(EntitiesInit.PROJECTILE_ROCK.get(), (EntityRendererManager entityRendererManager) -> new ProjectileRenderer(entityRendererManager, Minecraft.getInstance().getItemRenderer()));
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.FREEZE_BOLT.get(), FreezeBoltRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.PISTOL_BULLET.get(), PistolBulletRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.SHOTGUN_SHELL.get(), ShotgunShellRender::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityInit.WEB_SHOT.get(), WebShotRender::new);
    }
}