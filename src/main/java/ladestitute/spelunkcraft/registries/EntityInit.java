package ladestitute.spelunkcraft.registries;


import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.entities.ammo.EntityFreezeBolt;
import ladestitute.spelunkcraft.entities.ammo.EntityPistolBullet;
import ladestitute.spelunkcraft.entities.ammo.EntityShotgunShell;
import ladestitute.spelunkcraft.entities.ammo.EntityWebShot;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileFlare;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileMattockHead;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileRock;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileSkull;
import ladestitute.spelunkcraft.entities.tools.EntityBomb;
import ladestitute.spelunkcraft.entities.tools.EntityBouncyBomb;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class EntityInit {

    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, Spelunkcraft.MOD_ID);

    //Ammo
    public static final RegistryObject<EntityType<EntityFreezeBolt>> FREEZE_BOLT = ENTITIES.register("freeze_bolt",
            () -> EntityType.Builder.<EntityFreezeBolt>create(EntityFreezeBolt::new, EntityClassification.MISC).size(0.5F, 0.5F)
                    .build(new ResourceLocation(Spelunkcraft.MOD_ID, "textures/entity/ammo").toString()));
    public static final RegistryObject<EntityType<EntityPistolBullet>> PISTOL_BULLET = ENTITIES.register("pistol_bullet",
           () -> EntityType.Builder.<EntityPistolBullet>create(EntityPistolBullet::new, EntityClassification.MISC).size(0.5F, 0.5F)
                  .build(new ResourceLocation(Spelunkcraft.MOD_ID, "textures/entity/ammo").toString()));
     public static final RegistryObject<EntityType<EntityShotgunShell>> SHOTGUN_SHELL = ENTITIES.register("shotgun_shell",
           () -> EntityType.Builder.<EntityShotgunShell>create(EntityShotgunShell::new, EntityClassification.MISC).size(0.5F, 0.5F)
                 .build(new ResourceLocation(Spelunkcraft.MOD_ID, "textures/entity/ammo").toString()));
      public static final RegistryObject<EntityType<EntityWebShot>> WEB_SHOT = ENTITIES.register("web_shot",
                     () -> EntityType.Builder.<EntityWebShot>create(EntityWebShot::new, EntityClassification.MISC).size(0.5F, 0.5F)
               .build(new ResourceLocation(Spelunkcraft.MOD_ID, "textures/entity/ammo").toString()));

    //Projectiles
	public static final RegistryObject<EntityType<EntityProjectileRock>> PROJECTILE_ROCK = ENTITIES.register("projectile_rock",
            () -> EntityType.Builder.<EntityProjectileRock>create(EntityProjectileRock::new,
                    EntityClassification.MISC).size(0.25F, 0.25F).build("projectile_rock"));
    public static final RegistryObject<EntityType<EntityProjectileSkull>> PROJECTILE_SKULL = ENTITIES.register("projectile_skull",
            () -> EntityType.Builder.<EntityProjectileSkull>create(EntityProjectileSkull::new,
                    EntityClassification.MISC).size(0.25F, 0.25F).build("projectile_skull"));
    public static final RegistryObject<EntityType<EntityProjectileMattockHead>> PROJECTILE_MATTOCK_HEAD = ENTITIES.register("projectile_mattock_head",
            () -> EntityType.Builder.<EntityProjectileMattockHead>create(EntityProjectileMattockHead::new,
                    EntityClassification.MISC).size(0.25F, 0.25F).build("projectile_mattock_head"));
    public static final RegistryObject<EntityType<EntityProjectileFlare>> PROJECTILE_FLARE = ENTITIES.register("projectile_flare",
            () -> EntityType.Builder.<EntityProjectileFlare>create(EntityProjectileFlare::new,
                    EntityClassification.MISC).size(0.25F, 0.25F).build("projectile_flare"));

    //Bombs
    public static final RegistryObject<EntityType<EntityBomb>> BOMB = ENTITIES.register("bomb",
            () -> EntityType.Builder.<EntityBomb>create(EntityBomb::new,
                    EntityClassification.MISC).size(0.25F, 0.25F).build("bomb"));
    public static final RegistryObject<EntityType<EntityBouncyBomb>> BOUNCY_BOMB = ENTITIES.register("bouncy_bomb",
            () -> EntityType.Builder.<EntityBouncyBomb>create(EntityBouncyBomb::new,
                    EntityClassification.MISC).size(0.25F, 0.25F).build("bouncy_bomb"));
}
