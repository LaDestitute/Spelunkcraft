package ladestitute.spelunkcraft.registries;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.items.ammo.FreezeBolt;
import ladestitute.spelunkcraft.items.ammo.PistolBullet;
import ladestitute.spelunkcraft.items.ammo.ShotgunShell;
import ladestitute.spelunkcraft.items.ammo.WebShot;
import ladestitute.spelunkcraft.items.equipment.*;
import ladestitute.spelunkcraft.items.projectiles.ProjectileFlareItem;
import ladestitute.spelunkcraft.items.projectiles.ProjectileMattockHead;
import ladestitute.spelunkcraft.items.projectiles.ProjectileRock;
import ladestitute.spelunkcraft.items.projectiles.ProjectileSkull;
import ladestitute.spelunkcraft.items.tools.BombPasteItem;
import ladestitute.spelunkcraft.items.treasure.SmallRuby;
import ladestitute.spelunkcraft.items.tools.Bomb;
import ladestitute.spelunkcraft.items.tools.BouncyBomb;
import ladestitute.spelunkcraft.items.tools.RubyPickaxe;
import ladestitute.spelunkcraft.items.treasure.LargeRuby;
import ladestitute.spelunkcraft.items.weapons.*;
import ladestitute.spelunkcraft.util.enums.ModArmorMaterials;
import ladestitute.spelunkcraft.util.enums.ModItemTiers;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ItemInit {

    //Registry initialization
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            Spelunkcraft.MOD_ID);

    //Tools
    public static final RegistryObject<PickaxeItem> RUBY_PICKAXE = ITEMS.register("ruby_pickaxe",
            () -> new RubyPickaxe(ModItemTiers.RUBY, 4, 5.0f,
                    new Item.Properties().group(Spelunkcraft.TAB)));
    public static final RegistryObject<Item> BOMB_PASTE = ITEMS.register("bomb_paste",
            () -> new BombPasteItem(new Item.Properties().group(Spelunkcraft.TAB)));

    //Equipment
    public static final RegistryObject<ArmorItem> CLIMBING_GLOVES = ITEMS.register("climbing_gloves",
            () -> new ClimbingGloves(ModArmorMaterials.CLIMBING_GLOVES, EquipmentSlotType.OFFHAND,
                   new Item.Properties().maxStackSize(1).group(Spelunkcraft.GEAR_TAB)));
    public static final RegistryObject<ArmorItem> SPRING_SHOES = ITEMS.register("spring_shoes",
            () -> new SpringShoes(ModArmorMaterials.SPRING_SHOES, EquipmentSlotType.FEET,
                    new Item.Properties().group(Spelunkcraft.GEAR_TAB)));
    public static final RegistryObject<ArmorItem> SPECTACLES = ITEMS.register("spectacles",
            () -> new Spectacles(ModArmorMaterials.SPECTACLES, EquipmentSlotType.HEAD,
                    new Item.Properties().group(Spelunkcraft.GEAR_TAB)));
    public static final RegistryObject<ArmorItem> PITCHERS_MITT = ITEMS.register("pitchers_mitt",
            () -> new Cape(ModArmorMaterials.PITCHERS_MITT, EquipmentSlotType.OFFHAND,
                    new Item.Properties().group(Spelunkcraft.GEAR_TAB)));
    public static final RegistryObject<ArmorItem> CAPE = ITEMS.register("cape",
            () -> new Cape(ModArmorMaterials.CAPE, EquipmentSlotType.CHEST,
                    new Item.Properties().group(Spelunkcraft.GEAR_TAB)));
    public static final RegistryObject<ArmorItem> JETPACK = ITEMS.register("jetpack",
            () -> new Jetpack(ModArmorMaterials.JETPACK, EquipmentSlotType.CHEST,
                    new Item.Properties().group(Spelunkcraft.GEAR_TAB)));
    //Update the armor-material for Parachute, just trying to get this in already
    public static final RegistryObject<Item> PARACHUTE = ITEMS.register("parachute",
            () -> new ParachuteItem(new Item.Properties().group(Spelunkcraft.GEAR_TAB)));

    //Weapons
    public static final RegistryObject<SwordItem> WHIP = ITEMS.register("whip",
            () -> new Whip(ItemTier.STONE, 0, 0, new Item.Properties().group(Spelunkcraft.WEAP_TAB)));
    public static final RegistryObject<SwordItem> MACHETE = ITEMS.register("machete",
            () -> new Machete(ItemTier.IRON, 0, 0, new Item.Properties().group(Spelunkcraft.WEAP_TAB)));
    public static final RegistryObject<SwordItem> EMERALD_SWORD = ITEMS.register("emerald_sword",
            () -> new EmeraldSwordItem(ItemTier.STONE, 0, 0, new Item.Properties().group(Spelunkcraft.WEAP_TAB)));
    public static final RegistryObject<SwordItem> SAPPHIRE_SWORD = ITEMS.register("sapphire_sword",
            () -> new Machete(ItemTier.IRON, 0, 0, new Item.Properties().group(Spelunkcraft.WEAP_TAB)));
    public static final RegistryObject<SwordItem> RUBY_SWORD = ITEMS.register("ruby_sword",
            () -> new Machete(ItemTier.DIAMOND, 0, 0, new Item.Properties().group(Spelunkcraft.WEAP_TAB)));
    public static final RegistryObject<Item> FREEZE_RAY = ITEMS.register("freeze_ray",
            () -> new FreezeRay(new Item.Properties().group(Spelunkcraft.WEAP_TAB)));
    public static final RegistryObject<Item> PISTOL = ITEMS.register("pistol",
            () -> new Pistol(new Item.Properties().group(Spelunkcraft.WEAP_TAB)));
    public static final RegistryObject<Item> SHOTGUN = ITEMS.register("shotgun",
            () -> new Shotgun(new Item.Properties().group(Spelunkcraft.WEAP_TAB)));
    public static final RegistryObject<Item> WEB_GUN = ITEMS.register("web_gun",
            () -> new WebGun(new Item.Properties().group(Spelunkcraft.WEAP_TAB)));

    //Ammo
    public static final RegistryObject<Item> FREEZE_BOLT = ITEMS.register("freeze_bolt",
            () -> new FreezeBolt(new Item.Properties().group(Spelunkcraft.WEAP_TAB)));

    public static final RegistryObject<Item> PISTOL_BULLET = ITEMS.register("pistol_bullet",
            () -> new PistolBullet(new Item.Properties().group(Spelunkcraft.WEAP_TAB)));

    public static final RegistryObject<Item> SHOTGUN_SHELL = ITEMS.register("shotgun_shell",
            () -> new ShotgunShell(new Item.Properties().group(Spelunkcraft.WEAP_TAB)));

    public static final RegistryObject<Item> WEB_SHOT = ITEMS.register("web_shot",
            () -> new WebShot(new Item.Properties().group(Spelunkcraft.WEAP_TAB)));

    //Projectiles
    public static final RegistryObject<Item> PROJECTILE_ROCK = ITEMS.register("projectile_rock",
            () -> new ProjectileRock(new Item.Properties().group(Spelunkcraft.PROJ_TAB)));
    public static final RegistryObject<Item> PROJECTILE_SKULL = ITEMS.register("projectile_skull",
            () -> new ProjectileSkull(new Item.Properties().group(Spelunkcraft.PROJ_TAB)));
    public static final RegistryObject<Item> PROJECTILE_MATTOCK_HEAD = ITEMS.register("projectile_mattock_head",
            () -> new ProjectileMattockHead(new Item.Properties().group(Spelunkcraft.PROJ_TAB)));
    public static final RegistryObject<Item> PROJECTILE_FLARE = ITEMS.register("projectile_flare",
            () -> new ProjectileFlareItem(new Item.Properties().group(Spelunkcraft.PROJ_TAB)));

    //Treasure
    public static final RegistryObject<Item> SMALL_RUBY = ITEMS.register("small_ruby",
            () -> new SmallRuby(new Item.Properties().group(Spelunkcraft.TAB)));
    public static final RegistryObject<Item> LARGE_RUBY = ITEMS.register("large_ruby",
            () -> new LargeRuby(new Item.Properties().group(Spelunkcraft.TAB)));

    //Consumables
    //Royal Jelly is removed until 0.2, this is intended to be a straight port of 1.12
    //public static final RegistryObject<Item> TEST_ITEM = ITEMS.register("royal_jelly",
        //    () -> new RoyalJelly(new Item.Properties().group(Spelunkcraft.TAB).food(new Food.Builder().setAlwaysEdible().fastToEat().hunger(2)
            //        .saturation(1.0f).effect(new EffectInstance(Effects.ABSORPTION, 6000, 4), 1.0f).build())));

    // Utility Items
    public static final RegistryObject<Item> BOMB = ITEMS.register("bomb",
            () -> new Bomb(new Item.Properties().group(Spelunkcraft.UTILITY_ITEMS_TAB)));
    public static final RegistryObject<Item> BOUNCY_BOMB = ITEMS.register("bouncy_bomb",
            () -> new BouncyBomb(new Item.Properties().group(Spelunkcraft.UTILITY_ITEMS_TAB)));
}
