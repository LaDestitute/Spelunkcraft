package ladestitute.spelunkcraft.registries;

import ladestitute.spelunkcraft.Spelunkcraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class SoundInit {
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, Spelunkcraft.MOD_ID);

    //Equipment
    public static final RegistryObject<SoundEvent> SPRING = SOUNDS.register("item.equipment.spring", () -> new SoundEvent(new ResourceLocation(Spelunkcraft.MOD_ID, "item.equipment.spring")));

    //Weapons
    public static final RegistryObject<SoundEvent> RANGED_SHOTGUN = SOUNDS.register("entity.ranged.shotgun", () -> new SoundEvent(new ResourceLocation(Spelunkcraft.MOD_ID, "entity.ranged.shotgun")));
}
