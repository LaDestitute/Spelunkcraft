package ladestitute.spelunkcraft.registries;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraftforge.common.ToolType;

public class PropertiesInit {
    //Decor
    public static final Block.Properties FOSSIL = Block.Properties.create(Material.ROCK)
            .hardnessAndResistance(1.75f, 20.0f)
            .sound(SoundType.STONE)
            .harvestLevel(0)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool();
    public static final Block.Properties MINESHAFT = Block.Properties.create(Material.WOOD)
            .hardnessAndResistance(2f, 15.0f)
            .sound(SoundType.WOOD)
            .harvestLevel(0)
            .harvestTool(ToolType.AXE)
            .setRequiresTool();
    public static final Block.Properties VAULT = Block.Properties.create(Material.ROCK)
            .hardnessAndResistance(50f, 30.0f)
            .sound(SoundType.STONE)
            .harvestLevel(3)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool();


    //Ore
    public static final Block.Properties ORE = Block.Properties.create(Material.ROCK)
            .hardnessAndResistance(3.0f, 15.0f)
            .sound(SoundType.STONE)
            .harvestLevel(1)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool();

    //Traps
    public static final Block.Properties SPIKES = Block.Properties.create(Material.GLASS)
            .hardnessAndResistance(50.0f, 0.0f)
            .sound(SoundType.GLASS)
            .harvestLevel(3)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool()
            .noDrops();
    public static final Block.Properties PUSHBLOCK = Block.Properties.create(Material.ROCK)
            .hardnessAndResistance(50.0f, 30.0f)
            .sound(SoundType.STONE)
            .harvestLevel(3)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool();
    public static final Block.Properties POWDERBOX = Block.Properties.create(Material.IRON)
            .hardnessAndResistance(0.0f, 0.0f)
            .sound(SoundType.METAL)
            .harvestLevel(0)
            .harvestTool(ToolType.PICKAXE)
            .setRequiresTool()
            .noDrops();

    //World Blocks
    public static final Block.Properties FRAGILE_PROPERTY = Block.Properties.create(Material.ROCK).sound(SoundType.GLASS).hardnessAndResistance(0.0F, 0.0F);

    //Decor
    public static final Block.Properties PRECIOUSSOLID_PROPERTY = Block.Properties.create(Material.IRON).sound(SoundType.METAL).hardnessAndResistance(5.0F, 30.0F);

    //Util
    public static final Block.Properties BOMBBOX = Block.Properties.create(Material.WOOD)
            .hardnessAndResistance(4.0f, 15.0f)
            .sound(SoundType.WOOD)
            .harvestLevel(0)
            .setRequiresTool()
            .noDrops();
}
