package ladestitute.spelunkcraft.registries;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.blocks.decor.*;
import ladestitute.spelunkcraft.blocks.decor.slabs.*;
import ladestitute.spelunkcraft.blocks.ore.RubyOreBlock;
import ladestitute.spelunkcraft.blocks.decor.world.WorldPot;
import ladestitute.spelunkcraft.blocks.ore.SapphireOreBlock;
import ladestitute.spelunkcraft.blocks.traps.PowderboxBlock;
import ladestitute.spelunkcraft.blocks.traps.PushBlock;
import ladestitute.spelunkcraft.blocks.traps.SpikeBlock;
import ladestitute.spelunkcraft.blocks.AirLightSource;
import ladestitute.spelunkcraft.blocks.WaterLightSource;
import ladestitute.spelunkcraft.blocks.util.BombBoxBlock;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class BlockInit {

     //Registry initialization
     public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
             Spelunkcraft.MOD_ID);

     //Unobtainable and Util
     public static final RegistryObject<Block> AIR_LIGHT_SOURCE = BLOCKS.register("air_light_source",
             () -> new AirLightSource(Block.Properties.create(Material.AIR).setLightLevel(blockState ->
                     14).doesNotBlockMovement().noDrops()));
     public static final RegistryObject<Block> WATER_LIGHT_SOURCE = BLOCKS.register("water_light_source",
             () -> new WaterLightSource(Block.Properties.create(Material.OCEAN_PLANT).setLightLevel(blockState ->
                     14).doesNotBlockMovement().noDrops()));

     //Natural Blocks
     public static final RegistryObject<Block> FOSSIL = BLOCKS.register("fossil",
             () -> new FossilBlock(PropertiesInit.FOSSIL));
     public static final RegistryObject<Block> ORERUBY = BLOCKS.register("oreruby",
             () -> new RubyOreBlock(PropertiesInit.ORE));
     public static final RegistryObject<Block> ORESAPPHIRE = BLOCKS.register("oresapphire",
             () -> new SapphireOreBlock(PropertiesInit.ORE));

     //World Blocks
     public static final RegistryObject<Block> WORLD_POT = BLOCKS.register("world_pot", () -> new WorldPot(-0x72675B, PropertiesInit.FRAGILE_PROPERTY));

     //Decor
     public static final RegistryObject<Block> MINESHAFT_CORNER = BLOCKS.register("mineshaft_corner",
             () -> new MineshaftCornerBlock(PropertiesInit.MINESHAFT));
     public static final RegistryObject<Block> MINESHAFT_MIDDLE = BLOCKS.register("mineshaft_middle",
             () -> new MineshaftMiddleBlock(PropertiesInit.MINESHAFT));
     public static final RegistryObject<Block> MINESHAFT_BOTTOM = BLOCKS.register("mineshaft_bottom",
             () -> new MineshaftBottomBlock(PropertiesInit.MINESHAFT));
     public static final RegistryObject<Block> VAULT_CEILING = BLOCKS.register("vault_ceiling_block",
             () -> new VaultCeilingBlock(PropertiesInit.VAULT));
     public static final RegistryObject<Block> VAULT_MIDDLE = BLOCKS.register("vault_middle_block",
             () -> new VaultMiddleBlock(PropertiesInit.VAULT));
     public static final RegistryObject<Block> VAULT_CORNER = BLOCKS.register("vault_corner_block",
             () -> new VaultCornerBlock(PropertiesInit.VAULT));
     public static final RegistryObject<Block> SAPPHIREBLOCK = BLOCKS.register("blocksapphire",
             () -> new SapphireBlock(PropertiesInit.PRECIOUSSOLID_PROPERTY));
     public static final RegistryObject<Block> RUBYBLOCK = BLOCKS.register("blockruby",
             () -> new RubyBlock(PropertiesInit.PRECIOUSSOLID_PROPERTY));
     public static final RegistryObject<SlabBlock> DIAMOND_SLAB = BLOCKS.register("diamond_slab", () -> new DiamondSlab(PropertiesInit.PRECIOUSSOLID_PROPERTY));
     public static final RegistryObject<SlabBlock> EMERALD_SLAB = BLOCKS.register("emerald_slab", () -> new EmeraldSlab(PropertiesInit.PRECIOUSSOLID_PROPERTY));
     public static final RegistryObject<SlabBlock> GOLD_SLAB = BLOCKS.register("gold_slab", () -> new GoldSlab(PropertiesInit.PRECIOUSSOLID_PROPERTY));
     public static final RegistryObject<SlabBlock> RUBY_SLAB = BLOCKS.register("ruby_slab", () -> new RubySlab(PropertiesInit.PRECIOUSSOLID_PROPERTY));
     public static final RegistryObject<SlabBlock> SAPPHIRE_SLAB = BLOCKS.register("sapphire_slab", () -> new SapphireSlab(PropertiesInit.PRECIOUSSOLID_PROPERTY));

     //Utility
     public static final RegistryObject<Block> BOMBBOX = BLOCKS.register("bomb_box",
             () -> new BombBoxBlock(PropertiesInit.BOMBBOX));

     //Traps
     public static final RegistryObject<Block> SPIKES = BLOCKS.register("spikes",
             () -> new SpikeBlock(PropertiesInit.SPIKES));
     public static final RegistryObject<Block> PUSHBLOCK = BLOCKS.register("push_block",
             () -> new PushBlock(PropertiesInit.PUSHBLOCK));
     public static final RegistryObject<Block> POWDERBOX = BLOCKS.register("powder_box",
             () -> new PowderboxBlock(PropertiesInit.POWDERBOX));

     //Treasure

}
