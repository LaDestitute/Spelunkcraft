package ladestitute.spelunkcraft.items.projectiles;

import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileFlare;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileMattockHead;
import ladestitute.spelunkcraft.registries.ItemInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.List;

public class ProjectileFlareItem extends Item {
    public ProjectileFlareItem(Properties properties)
    {
        super(properties.maxStackSize(16));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent(TextFormatting.RED+"A portable source of light"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    // Method to do things on a right-click with the item, i.e launching projectile
    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        //This just gets the stack the player is holding.
        ItemStack stack = playerIn.getHeldItem(handIn);
        Vector3d look = playerIn.getLookVec();
        //Here I check if the world is the server, if so we can spawn the entity
        if(!worldIn.isRemote) {
            //Create a new instance of our entity
            EntityProjectileFlare entity = new EntityProjectileFlare(playerIn, worldIn);
            //This sets the entity to the item we are holding
            entity.setItem(stack);
            //This is the second most important method here, it sets how the entity shall shoot.
            //We're using func_234612 from Snowball instead as "entity.shoot" is kind of borked from my testing
            entity.func_234612_a_(playerIn, playerIn.rotationPitch, playerIn.rotationYaw, 0.0F, 1.5F, 1.0F);
            //Remove gravity if Pitcher's Mitt is in off-hand
            if(playerIn.getHeldItemOffhand().getItem() == ItemInit.PITCHERS_MITT.get())
            {
                entity.setNoGravity(true);
            }
            //This is the most important, it actually adds it to the world!
            worldIn.addEntity(entity);
        }

        //Check if not in creative, then if so: reduce the stack
        if(!playerIn.abilities.isCreativeMode) {
            stack.shrink(1);
        }

        //Then we can just return success for the method
        return ActionResult.resultSuccess(stack);
    }

}

