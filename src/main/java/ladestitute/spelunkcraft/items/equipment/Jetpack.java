package ladestitute.spelunkcraft.items.equipment;

import ladestitute.spelunkcraft.registries.ItemInit;
import ladestitute.spelunkcraft.util.helpers.KeyboardHelper;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.List;

public class Jetpack extends ArmorItem {
    public Jetpack(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

   // @Override
   // public void onArmorTick(ItemStack stack, World world, PlayerEntity playerIn) {
     //   if (KeyboardHelper.isHoldingZ() && playerIn.inventory.armorItemInSlot(2).getItem() == ItemInit.JETPACK.get())
       // {
        //    playerIn.addPotionEffect(new EffectInstance(Effects.LEVITATION, 1, 2));
         //   playerIn.addPotionEffect(new EffectInstance(Effects.SLOW_FALLING, 1, 2));
         //   playerIn.setMotion(0, +1, 0);
        //    stack.damageItem(1, playerIn, player -> {});
      //  }
  //  }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent(TextFormatting.BLUE+"The coolest way to travel. The fuel replenishes while you're on the ground. Hold Z in midair to use."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
