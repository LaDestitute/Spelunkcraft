package ladestitute.spelunkcraft.items.equipment;

import ladestitute.spelunkcraft.Spelunkcraft;
import ladestitute.spelunkcraft.registries.ItemInit;
import ladestitute.spelunkcraft.util.helpers.KeyboardHelper;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.List;

public class ClimbingGloves extends ArmorItem {
    public ClimbingGloves(IArmorMaterial materialIn, EquipmentSlotType slot, Properties builder) {
        super(materialIn, slot, builder);
    }

        @Override
        public void addInformation (ItemStack stack, World worldIn, List < ITextComponent > tooltip, ITooltipFlag flagIn)
        {
            tooltip.add(new StringTextComponent(TextFormatting.GREEN + "They protect your hands and let you cling to even the most slippery of walls. " +
                    "Hold Z while hugging a wall to use."));
            super.addInformation(stack, worldIn, tooltip, flagIn);
        }
    }


