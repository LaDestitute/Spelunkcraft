package ladestitute.spelunkcraft.items.equipment;

import ladestitute.spelunkcraft.registries.ItemInit;
import ladestitute.spelunkcraft.util.helpers.KeyboardHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.List;

public class ParachuteItem extends Item {
    public ParachuteItem(Properties properties) {
        super(properties.maxStackSize(1).maxDamage(1));
    }

    @Override
    public boolean onEntityItemUpdate(ItemStack stack, ItemEntity entity) {
        return false;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("It will deploy automatically as you're falling"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    //update

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        if(entityIn.fallDistance > 3) {
            PlayerEntity player = Minecraft.getInstance().player;
            if(!player.isCreative()) {
                stack.shrink(1);
                entityIn.fallDistance = 0.0F;
            }
            //stack.damageItem(1, entityIn, (LivingEntity) -> entityIn.sendBreakAnimation(EquipmentSlotType.HEAD));
        }

    }

}





