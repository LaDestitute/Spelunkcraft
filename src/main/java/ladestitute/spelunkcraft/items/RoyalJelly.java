package ladestitute.spelunkcraft.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class RoyalJelly extends Item {
    public RoyalJelly(Properties properties)
    {
        super(properties);
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A delicious nectar that's made by bees. Highly valued by gourmands everywhere."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
