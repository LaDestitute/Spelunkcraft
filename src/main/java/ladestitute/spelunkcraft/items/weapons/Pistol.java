package ladestitute.spelunkcraft.items.weapons;

import ladestitute.spelunkcraft.items.ammo.PistolBullet;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;
import java.util.function.Predicate;

public class Pistol extends BowItem {
    public Pistol(Properties properties)
    {
        super(properties.maxStackSize(1).maxDamage(144));
    }

    @Override
    public int getItemEnchantability()
    {
        return 0;
    }

    @Override
    public Predicate<ItemStack> getInventoryAmmoPredicate() {
        return getAmmoPredicate();
    }

    @Override
    public Predicate<ItemStack> getAmmoPredicate() {
        return itemStack -> itemStack.getItem() instanceof PistolBullet;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("A small handgun."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
