package ladestitute.spelunkcraft.items.tools;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import java.util.List;

public class BombPasteItem extends Item {
    public BombPasteItem(Properties properties) {
        super(properties.maxStackSize(8).setNoRepair().maxDamage(64));
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent(TextFormatting.WHITE + "Coat your bombs with this and they'll stick to pretty much anything."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public boolean hasContainerItem(ItemStack stack) {
        return true;
    }

    public boolean doesContainerItemLeaveCraftingGrid(ItemStack stack) {
        return false;
    }

    @Override
    public ItemStack getContainerItem(ItemStack itemstack) {
        ItemStack stack = itemstack.copy();
        if (stack.getMaxDamage() - stack.getDamage() <= 1) {
            stack.shrink(1);
        } else {
            stack.attemptDamageItem(1, random, null);
        }
        return stack;
    }

}
