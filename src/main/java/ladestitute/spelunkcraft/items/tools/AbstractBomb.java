package ladestitute.spelunkcraft.items.tools;

import ladestitute.spelunkcraft.entities.tools.AbstractEntityBomb;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;

public abstract class AbstractBomb extends Item {
	public AbstractBomb(Properties properties) {
		super(properties);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn)
	{
		worldIn.playSound(null, playerIn.getPosition(), SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.PLAYERS, 1.0F, 1.0F);
		ItemStack stack = playerIn.getHeldItem(handIn);

		// Only spawn the entity on the server
		if(!worldIn.isRemote)
		{
			AbstractEntityBomb entity = createEntity(playerIn, worldIn);
			entity.setItem(stack);
			shootEntity(playerIn, entity);
			worldIn.addEntity(entity);
		}

		// Don't subtract from the item count in creative mode
		if(!playerIn.abilities.isCreativeMode)
		{
			stack.shrink(1);
		}

		return ActionResult.resultSuccess(stack);
	}

	private void shootEntity(PlayerEntity player, ProjectileEntity entity) {
		// Use the same function as the snowball item does, for now
		entity.func_234612_a_(player, player.rotationPitch, player.rotationYaw, 0.0F, 1.5F, 1.0F);
	}

	protected abstract AbstractEntityBomb createEntity(PlayerEntity playerIn, World worldIn);
}
