package ladestitute.spelunkcraft.items.tools;

import java.util.List;

import ladestitute.spelunkcraft.entities.tools.AbstractEntityBomb;
import ladestitute.spelunkcraft.entities.tools.EntityBouncyBomb;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class BouncyBomb extends AbstractBomb {
	public BouncyBomb(Properties properties) {
		super(properties);
	}

	@Override
	protected AbstractEntityBomb createEntity(PlayerEntity playerIn, World worldIn) {
		return new EntityBouncyBomb(playerIn, worldIn);
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
	    tooltip.add(new StringTextComponent(TextFormatting.GREEN+"A bomb that has been coated with a highly elastic material"));
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}
}
