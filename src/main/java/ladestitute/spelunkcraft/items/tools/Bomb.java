package ladestitute.spelunkcraft.items.tools;

import java.util.List;

import ladestitute.spelunkcraft.entities.tools.AbstractEntityBomb;
import ladestitute.spelunkcraft.entities.tools.EntityBomb;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class Bomb extends AbstractBomb {
	public Bomb(Properties properties) {
		super(properties);
	}

	@Override
	protected AbstractEntityBomb createEntity(PlayerEntity playerIn, World worldIn) {
		return new EntityBomb(playerIn, worldIn);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
	    tooltip.add(new StringTextComponent(TextFormatting.DARK_GRAY+"A small explosive full of gunpowder, typically used for mining or making a quick getaway in a sticky situation"));
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}
}
