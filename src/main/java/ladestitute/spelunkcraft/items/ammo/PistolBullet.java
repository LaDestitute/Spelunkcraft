package ladestitute.spelunkcraft.items.ammo;

import ladestitute.spelunkcraft.entities.ammo.EntityPistolBullet;
import ladestitute.spelunkcraft.registries.SoundInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class PistolBullet extends ArrowItem {
    public PistolBullet(Properties properties)
    {
        super(properties.maxStackSize(32));
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        EntityPistolBullet arrow = new EntityPistolBullet(worldIn, shooter);
        shooter.playSound(SoundInit.RANGED_SHOTGUN.get(), 0.5F, 1.5F);
        return arrow;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Ammo for a pistol."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}