package ladestitute.spelunkcraft.items.ammo;

import ladestitute.spelunkcraft.entities.ammo.EntityPistolBullet;
import ladestitute.spelunkcraft.entities.ammo.EntityShotgunShell;
import ladestitute.spelunkcraft.registries.SoundInit;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

import java.util.List;

public class ShotgunShell extends ArrowItem {
    public ShotgunShell(Properties properties)
    {
        super(properties.maxStackSize(16));
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        EntityShotgunShell arrow = new EntityShotgunShell(worldIn, shooter);
        shooter.playSound(SoundInit.RANGED_SHOTGUN.get(), 0.5F, 1.5F);
        return arrow;
    }

    @Override
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Ammo for a shotgun."));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

}
