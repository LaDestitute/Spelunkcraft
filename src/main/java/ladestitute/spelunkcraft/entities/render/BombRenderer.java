package ladestitute.spelunkcraft.entities.render;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import ladestitute.spelunkcraft.entities.tools.AbstractEntityBomb;
import ladestitute.spelunkcraft.entities.tools.EntityBomb;
import ladestitute.spelunkcraft.registries.ItemInit;
import ladestitute.spelunkcraft.util.RenderUtil;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.SpriteRenderer;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Vector3f;

public class BombRenderer<T extends AbstractEntityBomb> extends SpriteRenderer<T> {
	public static final int INITIAL_FLASHING_RATE_IN_MILLISECONDS = 20 * 1000 / 60;
	public static final int RAPID_FLASHING_RATE_IN_MILLISECONDS = 4 * 1000 / 60;

	private ItemRenderer itemRenderer;

	public BombRenderer(EntityRendererManager renderManagerIn, ItemRenderer itemRendererIn) {
		super(renderManagerIn, itemRendererIn);
		this.itemRenderer = itemRendererIn;
	}


   @Override
   public void render(T entityIn, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
   {
	   float modulationValue = getFlashingModulationValue(entityIn.getCreationTime(), entityIn.shouldFlashRapidly() ? RAPID_FLASHING_RATE_IN_MILLISECONDS : INITIAL_FLASHING_RATE_IN_MILLISECONDS);
	   RenderUtil.renderProjectileEntityWithModulation(entityIn, matrixStackIn, bufferIn, packedLightIn, renderManager, itemRenderer, 1.0F, modulationValue, modulationValue);
   }

   private float getFlashingModulationValue(Instant baseTimestamp, int frequencyInMilliSeconds) {
	   Instant now = Instant.now();
	   long millisSince = Duration.between(baseTimestamp, now).toMillis();
	   // This creates a nice sine wave
	   double modValue = (Math.sin(((millisSince * 2.0) / frequencyInMilliSeconds - 0.5) * Math.PI) + 1) / 2.0;
	   int colorModValue = (int) (255 * modValue);
	   return colorModValue;
   }
}
