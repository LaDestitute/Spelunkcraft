package ladestitute.spelunkcraft.entities.ammo;

import ladestitute.spelunkcraft.registries.EntityInit;
import ladestitute.spelunkcraft.registries.ItemInit;
import ladestitute.spelunkcraft.registries.SoundInit;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityShotgunShell extends AbstractArrowEntity {

    public EntityShotgunShell(EntityType<EntityShotgunShell> type, World world) {
        super(type, world);

    }

    public EntityShotgunShell(World worldIn, LivingEntity shooter) {
        super((EntityType<? extends AbstractArrowEntity>) EntityInit.SHOTGUN_SHELL.get(), shooter, worldIn);
        this.setDamage(12D);
        this.playSound(SoundInit.RANGED_SHOTGUN.get(), 0.5F, 1.0F);
    }

    public EntityShotgunShell(World worldIn, double x, double y, double z) {
        super((EntityType<? extends AbstractArrowEntity>) EntityInit.SHOTGUN_SHELL.get(), x, y, z, worldIn);
    }

    @Override
    public void setHitSound(SoundEvent soundIn) {
        SoundInit.RANGED_SHOTGUN.get();
    }

    @Override
    protected ItemStack getArrowStack() {
        return new ItemStack(ItemInit.SHOTGUN_SHELL.get());
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void tick() {
        super.tick();
        if (!this.world.isRemote)
        {
            if (this.ticksExisted == 1)
            {
                this.playSound(SoundInit.RANGED_SHOTGUN.get(), 0.5F, 1.5F);
            }
        }

    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);

    }
}
