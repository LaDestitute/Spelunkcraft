package ladestitute.spelunkcraft.entities.projectiles;

import ladestitute.spelunkcraft.registries.EntityInit;
import ladestitute.spelunkcraft.registries.ItemInit;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.projectile.ProjectileItemEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityProjectileRock extends ProjectileItemEntity {

    // Three constructors, also make sure not to miss this line when altering it for copy-pasting
    public EntityProjectileRock(EntityType<EntityProjectileRock> type, World world) {
        super(type, world);
    }

    public EntityProjectileRock(LivingEntity entity, World world) {
        super(EntityInit.PROJECTILE_ROCK.get(), entity, world);
    }

    public EntityProjectileRock(double x, double y, double z, World world) {
        super(EntityInit.PROJECTILE_ROCK.get(), x, y, z, world);
    }

    // Get the item that the projectile is thrown from, blocks require ".asItem()" as well
    @Override
    protected Item getDefaultItem() {
        return ItemInit.PROJECTILE_ROCK.get().asItem();
    }

    // Spawns the entity, just as important as the above method
    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    // A method to do things on entity or block-hit
    @Override
    protected void onImpact(RayTraceResult result) {
        //This line is checking the type of RayTraceResult, in this case
        //it will be when it hits and entity
        if (result.getType() == RayTraceResult.Type.ENTITY) {
            //This is a variable that we have set, it gets the entity from the RayTraceResult.
            //We cast it to EntityRayTraceResult, just to ensure that it is infact an entity.
            Entity entity = ((EntityRayTraceResult) result).getEntity();
            //This integer is the damage value that it gives to the entity when it is hit
            //I haven't initialized it here as I will do that below.
            float damage;
            damage = 1.5F;
            ItemStack stack1 = new ItemStack(ItemInit.PROJECTILE_ROCK.get());
            ItemEntity rock = new ItemEntity(this.getEntityWorld(), this.getPosX(), this.getPosY() + 1, this.getPosZ(), stack1);
            world.addEntity(rock);
            //Get the entity, the thrower and the damage amount
            World world = Minecraft.getInstance().world;
            LivingEntity playerIn = world.getClosestPlayer(this, 1);
            entity.attackEntityFrom(DamageSource.causeThrownDamage(this, playerIn), (float) damage);

            //After hitting the entity, we need to remove it from the world, otherwise it will keep flying the air.
            //We first use if to check if the world is the server by using isRemote.
            if (!world.isRemote) {
                //If it is the server then it will remove this (the entity) from the world.
                this.remove();

                //I have put this here as I want to remove it from the world after it has hit the entity.
                //if you wanted it to go through entities then you could remove this and put it later.
            }
        }

        //Just like before this checks the result and if it hits a block this code will run
        if (result.getType() == RayTraceResult.Type.BLOCK) {
            ItemStack stack1 = new ItemStack(ItemInit.PROJECTILE_ROCK.get());
            ItemEntity rock = new ItemEntity(this.getEntityWorld(), this.getPosX(), this.getPosY() + 1, this.getPosZ(), stack1);
            world.addEntity(rock);
            this.remove();
            //Now we get the BlockRayTraceResult from the result
            //Casting it to the BlockRayTraceResult.
            BlockRayTraceResult blockRTR = (BlockRayTraceResult) result;

            //I have checked to see if it hits the top of the block

            //    if (blockRTR.getFace() == Direction.UP) {
            //Then I have added a small check here to only allow something to happen when it
            //Hits a grass block
            //  if (world.getBlockState(blockRTR.getPos()) == Blocks.GRASS_BLOCK.getDefaultState()) {
            //This gets the world, and then sets the blockstate of the position of the entity
            //and the blockstate
            //  world.setBlockState(this.getOnPosition(), Blocks.STONE.getDefaultState());
            //     }

            //And just incase non of these are true, I am removing it from the world.
            if (!world.isRemote) {
                this.remove();
            }
        }
    }
}