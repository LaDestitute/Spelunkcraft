package ladestitute.spelunkcraft.entities.tools;

import ladestitute.spelunkcraft.registries.EntityInit;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.world.World;

public class EntityBouncyBomb extends AbstractEntityBomb {
	private static final float SECONDS_TO_EXPLODE = 3.88f;
	private static final float SECONDS_TO_FLASH_RAPIDLY = 2.67f;
	private static final int EXPLOSION_POWER = 3;
	private static final double BOUNCE_DAMPENING_FACTOR = 0.50;

	public EntityBouncyBomb(EntityType<EntityBouncyBomb> type, World world) {
        super(type, world, SECONDS_TO_EXPLODE, SECONDS_TO_FLASH_RAPIDLY, EXPLOSION_POWER, BOUNCE_DAMPENING_FACTOR);
	}

    public EntityBouncyBomb(LivingEntity shooter, World world) {
        super(EntityInit.BOUNCY_BOMB.get(), shooter, world, SECONDS_TO_EXPLODE, SECONDS_TO_FLASH_RAPIDLY, EXPLOSION_POWER, BOUNCE_DAMPENING_FACTOR);
	}
}
