package ladestitute.spelunkcraft.blocks;

import net.minecraft.block.*;

import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;

public class AirLightSource extends LightSource {
    //Our helper-block for light sources in air
    public AirLightSource(AbstractBlock.Properties properties) {
        super(properties);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.INVISIBLE;
    }

    @Override
    public boolean isAir(BlockState state) {
        return true;
    }
}
