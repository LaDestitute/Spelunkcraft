package ladestitute.spelunkcraft.blocks;

import net.minecraft.block.*;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.server.ServerWorld;

import javax.annotation.Nullable;
import java.util.Random;

public class LightSource extends Block {
    //Our origin-class for our two types of helper light-blocks

    public static final IntegerProperty LIGHTVALUE = IntegerProperty.create("light_value", 0, 15);

    public LightSource(Block.Properties properties) {
        super(properties.notSolid());
        this.setDefaultState(this.stateContainer.getBaseState().with(LIGHTVALUE, Integer.valueOf(0)));
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return VoxelShapes.empty();
    }

    public int getLightValue(BlockState state) {
        return state.get(LIGHTVALUE);
    }

    public BlockState getStateWithLightValue(int lightValue) {
        return this.getDefaultState().with(LIGHTVALUE, Integer.valueOf(lightValue));
    }

    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(LIGHTVALUE);
        // builder.add(WATERLOGGED);
        super.fillStateContainer(builder);
    }

}