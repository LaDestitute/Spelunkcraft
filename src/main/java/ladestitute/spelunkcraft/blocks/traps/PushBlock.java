package ladestitute.spelunkcraft.blocks.traps;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FallingBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.util.List;

public class PushBlock extends FallingBlock {

    public PushBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent(TextFormatting.GRAY + "A carved stone block. It's very heavy and seems to be a major crushing hazard"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    @Override
    protected void onStartFalling(FallingBlockEntity fallingEntity) {
        super.onStartFalling(fallingEntity);
    }

    @Override
    public void onEntityCollision(BlockState state, World worldIn, BlockPos pos, Entity entityIn) {
        if (!worldIn.isRemote && entityIn instanceof LivingEntity)
        {
            entityIn.attackEntityFrom(DamageSource.GENERIC, 99.0F);
            //worldIn.playSound(null, entityIn.getPosition(), SoundsHandler.PUSHBLOCKCRUSH, SoundCategory.PLAYERS, 1.0F, 1.0F);
            //damageSource.mess
        }

    }
}
