package ladestitute.spelunkcraft.blocks.traps;

import net.minecraft.block.*;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.command.impl.data.BlockDataAccessor;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.util.List;

public class SpikeBlock extends FallingBlock {

    public SpikeBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent(TextFormatting.WHITE+"These protrusions will leave big holes in anyone unlucky enough to fall on them"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    //Our custom Bounding Box
    protected static final VoxelShape SPIKES_SHAPE = Block.makeCuboidShape(0D, 0D, 0D, 1.0D, 0.21875D, 1.0D);

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        return SPIKES_SHAPE;
    }

    //As of +1.13, Cutout/etc rendering has moved to event subscriber

    //Falling onto Spikes will kill the player, dealing 49.5 hearts of damage
    //This requires having a custom bounding box, which is used for entity detection
    //public void onEntityWalk(World worldIn, BlockPos pos, Entity entityIn)  will also work
    public void onFallenUpon(World worldIn, BlockPos pos, Entity entityIn, float fallDistance)
    {
        entityIn.attackEntityFrom(DamageSource.GENERIC, 99.0F);
        //  worldIn.playSound(null, entityIn.getPosition(), SoundsHandler.SPIKEHIT, SoundCategory.PLAYERS, 1.0F, 1.0F);
    }

    //public void onEntityWalk(World worldIn, BlockPos pos, Entity entityIn)
    //{
    //	entityIn.attackEntityFrom(DamageSource.GENERIC, 99.0F);
    //}

}
