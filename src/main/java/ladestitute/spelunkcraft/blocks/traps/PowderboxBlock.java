package ladestitute.spelunkcraft.blocks.traps;

import ladestitute.spelunkcraft.SpelunkConfig;
import ladestitute.spelunkcraft.entities.ammo.EntityFreezeBolt;
import ladestitute.spelunkcraft.entities.ammo.EntityPistolBullet;
import ladestitute.spelunkcraft.entities.ammo.EntityShotgunShell;
import ladestitute.spelunkcraft.entities.ammo.EntityWebShot;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileFlare;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileMattockHead;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileRock;
import ladestitute.spelunkcraft.entities.projectiles.EntityProjectileSkull;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FallingBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.FallingBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.util.List;

public class PowderboxBlock extends FallingBlock {
    float pbstr = 8F;

    public PowderboxBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent(TextFormatting.RED+"It's filled with black powder. Handle with care"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    public boolean canDropFromExplosion(Explosion explosionIn)
    {
        return false;
    }

    /**
     * Calling for entity-checks (thrown projectile only)
     */

    public void onEntityCollision(World worldIn, BlockPos pos, BlockState state, Entity entityIn)
    {
        if (!worldIn.isRemote && entityIn instanceof ArrowEntity ||
                !worldIn.isRemote && entityIn instanceof EntityProjectileMattockHead ||
                //!worldIn.isRemote && entityIn instanceof EntityProjectileRope ||
                !worldIn.isRemote && entityIn instanceof EntityProjectileRock ||
                !worldIn.isRemote && entityIn instanceof EntityProjectileFlare ||
                !worldIn.isRemote && entityIn instanceof EntityProjectileSkull ||
                !worldIn.isRemote && entityIn instanceof EntityFreezeBolt ||
                !worldIn.isRemote && entityIn instanceof EntityPistolBullet ||
                !worldIn.isRemote && entityIn instanceof EntityShotgunShell ||
                !worldIn.isRemote && entityIn instanceof EntityWebShot)
        {
            if(SpelunkConfig.ExplosiveGriefing.get() == true)
            {
                entityIn.world.createExplosion(entityIn, entityIn.getPosition().getX(), entityIn.getPosition().getY(), entityIn.getPosition().getZ(), pbstr, Explosion.Mode.DESTROY);
                worldIn.removeBlock(pos, false);
            }
            else
            {
                entityIn.world.createExplosion(entityIn, entityIn.getPosition().getX(), entityIn.getPosition().getY(), entityIn.getPosition().getZ(), pbstr, Explosion.Mode.NONE);
                worldIn.removeBlock(pos, false);
            }
        }

    }

    /**
     * Called after the block is set in the Chunk data, but before the Tile Entity is set
     */
    public void onBlockAdded(World worldIn, BlockPos pos, BlockState state)
    {
        if (!worldIn.isRemote)
        {
            if (worldIn.isBlockPowered(pos))
            {
                if (!worldIn.isRemote)
                {
                    if(SpelunkConfig.ExplosiveGriefing.get() == true)
                    {
                        worldIn.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), pbstr, Explosion.Mode.DESTROY);
                        worldIn.removeBlock(pos, false);
                    }
                    else
                    {
                        worldIn.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), pbstr, Explosion.Mode.NONE);
                        worldIn.removeBlock(pos, false);
                    }
                }
            }
        }
    }

    @Override
    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        // TODO Auto-generated method stub
        if (!worldIn.isRemote)
        {
            if (worldIn.isBlockPowered(pos))
            {
                if (!worldIn.isRemote)
                {
                    if(SpelunkConfig.ExplosiveGriefing.get() == true)
                    {
                        worldIn.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), pbstr, Explosion.Mode.DESTROY);
                        worldIn.removeBlock(pos, false);
                    }
                    else
                    {
                        worldIn.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), pbstr, Explosion.Mode.NONE);
                        worldIn.removeBlock(pos, false);
                    }
                }
            }
        }
    }

    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        ItemStack itemstack = player.getHeldItem(handIn);
        Item item = itemstack.getItem();
        if (item != Items.FLINT_AND_STEEL && item != Items.FIRE_CHARGE) {
            return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
        } else {
            worldIn.removeBlock(pos, false);
          //worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 11);
            if(SpelunkConfig.ExplosiveGriefing.get() == true)
            {
                player.world.createExplosion(player, player.getPosition().getX(), player.getPosition().getY(),
                        player.getPosition().getZ(), pbstr, Explosion.Mode.DESTROY);
                worldIn.removeBlock(pos, false);
            }
            else
            {
                player.world.createExplosion(player, player.getPosition().getX(), player.getPosition().getY(),
                        player.getPosition().getZ(), pbstr, Explosion.Mode.NONE);
                worldIn.removeBlock(pos, false);
            }
            if (!player.isCreative()) {
                if (item == Items.FLINT_AND_STEEL) {
                    itemstack.damageItem(1, player, (player1) -> {
                        player1.sendBreakAnimation(handIn);
                    });
                } else {
                    itemstack.shrink(1);
                }
            }

            return ActionResultType.func_233537_a_(worldIn.isRemote);
        }
    }

    /*** Called when this Block is destroyed by an Explosion
     */
    public void onExplosionDestroy(World worldIn, BlockPos pos, Explosion explosionIn)
    {
        if (!worldIn.isRemote)
        {
            if(SpelunkConfig.ExplosiveGriefing.get() == true)
            {
                worldIn.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), pbstr, Explosion.Mode.DESTROY);
                worldIn.removeBlock(pos, false);
            }
            else
            {
                worldIn.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), pbstr, Explosion.Mode.NONE);
                worldIn.removeBlock(pos, false);
            }
        }
    }

    }