package ladestitute.spelunkcraft.blocks.decor.world;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FallingBlock;
import net.minecraft.block.HorizontalBlock;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.Direction;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;

import java.util.List;
import java.util.stream.Stream;

public class WorldPot extends FallingBlock {

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Todo text"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }

    public static final DirectionProperty FACING = HorizontalBlock.HORIZONTAL_FACING;

    private static final VoxelShape SHAPE_N = Stream.of(Block.makeCuboidShape(8, 0, 8, 9, 1, 9),
            Block.makeCuboidShape(8, 0, 7, 9, 1, 8),
            Block.makeCuboidShape(7, 0, 7, 8, 1, 8),
            Block.makeCuboidShape(7, 0, 8, 8, 1, 9),
            Block.makeCuboidShape(7, 1, 9, 8, 2, 10),
            Block.makeCuboidShape(8, 1, 9, 9, 2, 10),
            Block.makeCuboidShape(9, 1, 8, 10, 2, 9),
            Block.makeCuboidShape(9, 1, 7, 10, 2, 8),
            Block.makeCuboidShape(8, 1, 6, 9, 2, 7),
            Block.makeCuboidShape(7, 1, 6, 8, 2, 7),
            Block.makeCuboidShape(6, 1, 8, 7, 2, 9),
            Block.makeCuboidShape(6, 1, 7, 7, 2, 8),
            Block.makeCuboidShape(6, 3, 7, 7, 4, 8),
            Block.makeCuboidShape(8, 3, 9, 9, 4, 10),
            Block.makeCuboidShape(7, 3, 9, 8, 4, 10),
            Block.makeCuboidShape(6, 3, 8, 7, 4, 9),
            Block.makeCuboidShape(8, 3, 6, 9, 4, 7),
            Block.makeCuboidShape(7, 3, 6, 8, 4, 7),
            Block.makeCuboidShape(9, 3, 8, 10, 4, 9),
            Block.makeCuboidShape(9, 3, 7, 10, 4, 8),
            Block.makeCuboidShape(6, 2, 6, 7, 3, 7),
            Block.makeCuboidShape(7, 2, 5, 8, 3, 6),
            Block.makeCuboidShape(8, 2, 5, 9, 3, 6),
            Block.makeCuboidShape(9, 2, 6, 10, 3, 7),
            Block.makeCuboidShape(10, 2, 7, 11, 3, 8),
            Block.makeCuboidShape(10, 2, 8, 11, 3, 9),
            Block.makeCuboidShape(9, 2, 9, 10, 3, 10),
            Block.makeCuboidShape(8, 2, 10, 9, 3, 11),
            Block.makeCuboidShape(7, 2, 10, 8, 3, 11),
            Block.makeCuboidShape(6, 2, 9, 7, 3, 10),
            Block.makeCuboidShape(5, 2, 8, 6, 3, 9),
            Block.makeCuboidShape(5, 2, 7, 6, 3, 8)).reduce((v1, v2) -> {
        return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);
    }).get();
    private static final VoxelShape SHAPE_W = Stream.of(Block.makeCuboidShape(8, 0, 8, 9, 1, 9),
            Block.makeCuboidShape(8, 0, 7, 9, 1, 8),
            Block.makeCuboidShape(7, 0, 7, 8, 1, 8),
            Block.makeCuboidShape(7, 0, 8, 8, 1, 9),
            Block.makeCuboidShape(7, 1, 9, 8, 2, 10),
            Block.makeCuboidShape(8, 1, 9, 9, 2, 10),
            Block.makeCuboidShape(9, 1, 8, 10, 2, 9),
            Block.makeCuboidShape(9, 1, 7, 10, 2, 8),
            Block.makeCuboidShape(8, 1, 6, 9, 2, 7),
            Block.makeCuboidShape(7, 1, 6, 8, 2, 7),
            Block.makeCuboidShape(6, 1, 8, 7, 2, 9),
            Block.makeCuboidShape(6, 1, 7, 7, 2, 8),
            Block.makeCuboidShape(6, 3, 7, 7, 4, 8),
            Block.makeCuboidShape(8, 3, 9, 9, 4, 10),
            Block.makeCuboidShape(7, 3, 9, 8, 4, 10),
            Block.makeCuboidShape(6, 3, 8, 7, 4, 9),
            Block.makeCuboidShape(8, 3, 6, 9, 4, 7),
            Block.makeCuboidShape(7, 3, 6, 8, 4, 7),
            Block.makeCuboidShape(9, 3, 8, 10, 4, 9),
            Block.makeCuboidShape(9, 3, 7, 10, 4, 8),
            Block.makeCuboidShape(6, 2, 6, 7, 3, 7),
            Block.makeCuboidShape(7, 2, 5, 8, 3, 6),
            Block.makeCuboidShape(8, 2, 5, 9, 3, 6),
            Block.makeCuboidShape(9, 2, 6, 10, 3, 7),
            Block.makeCuboidShape(10, 2, 7, 11, 3, 8),
            Block.makeCuboidShape(10, 2, 8, 11, 3, 9),
            Block.makeCuboidShape(9, 2, 9, 10, 3, 10),
            Block.makeCuboidShape(8, 2, 10, 9, 3, 11),
            Block.makeCuboidShape(7, 2, 10, 8, 3, 11),
            Block.makeCuboidShape(6, 2, 9, 7, 3, 10),
            Block.makeCuboidShape(5, 2, 8, 6, 3, 9),
            Block.makeCuboidShape(5, 2, 7, 6, 3, 8)).reduce((v1, v2) -> {
        return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);
    }).get();
    private static final VoxelShape SHAPE_S = Stream.of(Block.makeCuboidShape(8, 0, 8, 9, 1, 9),
            Block.makeCuboidShape(8, 0, 7, 9, 1, 8),
            Block.makeCuboidShape(7, 0, 7, 8, 1, 8),
            Block.makeCuboidShape(7, 0, 8, 8, 1, 9),
            Block.makeCuboidShape(7, 1, 9, 8, 2, 10),
            Block.makeCuboidShape(8, 1, 9, 9, 2, 10),
            Block.makeCuboidShape(9, 1, 8, 10, 2, 9),
            Block.makeCuboidShape(9, 1, 7, 10, 2, 8),
            Block.makeCuboidShape(8, 1, 6, 9, 2, 7),
            Block.makeCuboidShape(7, 1, 6, 8, 2, 7),
            Block.makeCuboidShape(6, 1, 8, 7, 2, 9),
            Block.makeCuboidShape(6, 1, 7, 7, 2, 8),
            Block.makeCuboidShape(6, 3, 7, 7, 4, 8),
            Block.makeCuboidShape(8, 3, 9, 9, 4, 10),
            Block.makeCuboidShape(7, 3, 9, 8, 4, 10),
            Block.makeCuboidShape(6, 3, 8, 7, 4, 9),
            Block.makeCuboidShape(8, 3, 6, 9, 4, 7),
            Block.makeCuboidShape(7, 3, 6, 8, 4, 7),
            Block.makeCuboidShape(9, 3, 8, 10, 4, 9),
            Block.makeCuboidShape(9, 3, 7, 10, 4, 8),
            Block.makeCuboidShape(6, 2, 6, 7, 3, 7),
            Block.makeCuboidShape(7, 2, 5, 8, 3, 6),
            Block.makeCuboidShape(8, 2, 5, 9, 3, 6),
            Block.makeCuboidShape(9, 2, 6, 10, 3, 7),
            Block.makeCuboidShape(10, 2, 7, 11, 3, 8),
            Block.makeCuboidShape(10, 2, 8, 11, 3, 9),
            Block.makeCuboidShape(9, 2, 9, 10, 3, 10),
            Block.makeCuboidShape(8, 2, 10, 9, 3, 11),
            Block.makeCuboidShape(7, 2, 10, 8, 3, 11),
            Block.makeCuboidShape(6, 2, 9, 7, 3, 10),
            Block.makeCuboidShape(5, 2, 8, 6, 3, 9),
            Block.makeCuboidShape(5, 2, 7, 6, 3, 8)).reduce((v1, v2) -> {
        return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);
    }).get();
    private static final VoxelShape SHAPE_E = Stream.of(Block.makeCuboidShape(8, 0, 8, 9, 1, 9),
            Block.makeCuboidShape(8, 0, 7, 9, 1, 8),
            Block.makeCuboidShape(7, 0, 7, 8, 1, 8),
            Block.makeCuboidShape(7, 0, 8, 8, 1, 9),
            Block.makeCuboidShape(7, 1, 9, 8, 2, 10),
            Block.makeCuboidShape(8, 1, 9, 9, 2, 10),
            Block.makeCuboidShape(9, 1, 8, 10, 2, 9),
            Block.makeCuboidShape(9, 1, 7, 10, 2, 8),
            Block.makeCuboidShape(8, 1, 6, 9, 2, 7),
            Block.makeCuboidShape(7, 1, 6, 8, 2, 7),
            Block.makeCuboidShape(6, 1, 8, 7, 2, 9),
            Block.makeCuboidShape(6, 1, 7, 7, 2, 8),
            Block.makeCuboidShape(6, 3, 7, 7, 4, 8),
            Block.makeCuboidShape(8, 3, 9, 9, 4, 10),
            Block.makeCuboidShape(7, 3, 9, 8, 4, 10),
            Block.makeCuboidShape(6, 3, 8, 7, 4, 9),
            Block.makeCuboidShape(8, 3, 6, 9, 4, 7),
            Block.makeCuboidShape(7, 3, 6, 8, 4, 7),
            Block.makeCuboidShape(9, 3, 8, 10, 4, 9),
            Block.makeCuboidShape(9, 3, 7, 10, 4, 8),
            Block.makeCuboidShape(6, 2, 6, 7, 3, 7),
            Block.makeCuboidShape(7, 2, 5, 8, 3, 6),
            Block.makeCuboidShape(8, 2, 5, 9, 3, 6),
            Block.makeCuboidShape(9, 2, 6, 10, 3, 7),
            Block.makeCuboidShape(10, 2, 7, 11, 3, 8),
            Block.makeCuboidShape(10, 2, 8, 11, 3, 9),
            Block.makeCuboidShape(9, 2, 9, 10, 3, 10),
            Block.makeCuboidShape(8, 2, 10, 9, 3, 11),
            Block.makeCuboidShape(7, 2, 10, 8, 3, 11),
            Block.makeCuboidShape(6, 2, 9, 7, 3, 10),
            Block.makeCuboidShape(5, 2, 8, 6, 3, 9),
            Block.makeCuboidShape(5, 2, 7, 6, 3, 8)).reduce((v1, v2) -> {
        return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);
    }).get();

    public WorldPot(int i, Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(FACING, Direction.NORTH));
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch(state.get(FACING)) {
            case NORTH: return SHAPE_N;
            case SOUTH: return SHAPE_S;
            case EAST: return SHAPE_E;
            case WEST: return SHAPE_W;
            default: return SHAPE_N;

        }
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

    //Deprecated
    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(FACING)));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(FACING);
    }

}
