package ladestitute.spelunkcraft.blocks.decor;

import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;

import java.util.List;

public class SapphireBlock extends Block {

    public SapphireBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent("Placeholder text"));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}

